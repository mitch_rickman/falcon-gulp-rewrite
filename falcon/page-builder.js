var extend1 = function(child, parent) {
  for (var key in parent) {
    if (hasProp.call(parent, key)) child[key] = parent[key];
  }
  function ctor() {
    this.constructor = child;
  }
  ctor.prototype = parent.prototype;
  child.prototype = new ctor();
  child.__super__ = parent.prototype; return child;
}
var hasProp = {}.hasOwnProperty;

var _ = require('lodash');
var fs = require('fs');
var path = require('path');
var extend = require('extend');
var lazypipe = require('lazypipe');
var tap = require('gulp-tap');
var jade = require('gulp-jade');
var compiler = require('gulp-jade/node_modules/jade');
var log = require('@14four/falcon/lib/logger');
var Builder = require('@14four/falcon/lib/builder');

PageBuilder = (function(superClass) {
  extend1(PageBuilder, superClass);

  function PageBuilder(isRelease, root, forcedDependencies) {
    var builder, forced;
    builder = this;
    this.currentFile = null;
    this.isRelease = isRelease;
    this.root = root;
    if (forcedDependencies != null) {
      forced = forcedDependencies.map(function(d) {
        return path.resolve(builder.root, d);
      });
    } else {
      forced = [];
    }
    this.config = {
      pretty: !isRelease,
      compileDebug: !isRelease,
      basedir: this.root,
      locals: {
        json: (function(_this) {
          return function(dataPath) {
            return _this.data(dataPath);
          };
        })(this),
        jadePartial: (function(_this) {
          return function(jadePath, jadeData, jadeConfig) {
            return _this.jadePartial(jadePath, jadeData, jadeConfig);
          };
        })(this),
        isDebug: (function(_this) {
          return function() {
            return !_this.isRelease;
          };
        })(this),
        isRelease: (function(_this) {
          return function() {
            return _this.isRelease;
          };
        })(this),
        env: (function(_this) {
          return function() {
            if (_this.isRelease) {
              return 'release';
            } else {
              return 'debug';
            }
          };
        })(this),
        "export": function() {
          var dataPaths, deps, pageDependencies, pageForcedPaths;
          pageDependencies = this.dependencies[this.page];
          if (pageDependencies != null) {
            dataPaths = pageDependencies.data.map(function(p) {
              return path.resolve(builder.root, p);
            });
            pageForcedPaths = pageDependencies.forced.map(function(p) {
              return path.resolve(builder.root, p);
            });
            deps = _.union(forced, pageDependencies.modules, pageDependencies.components, dataPaths, pageForcedPaths);
            log.yellow('Page Builder : Page Dependencies', this.page, '\n', deps);
            return builder.emit('dependencies', builder.currentFile, deps);
          } else {
            return builder.emit('dependencies', builder.currentFile, forced);
          }
        }
      }
    };
    this.task = lazypipe().pipe(tap, (function(_this) {
      return function(file) {
        var filename;
        _this.currentFile = file.path;
        filename = path.basename(file.path, '.jade');
        return _this.config.locals.page = filename;
      };
    })(this)).pipe(jade, this.config);
    return;
  }

  PageBuilder.prototype.getConfig = function() {
    return this.config;
  };

  PageBuilder.prototype.getTask = function() {
    return this.task;
  };

  PageBuilder.prototype.jadePartial = function(jadePath, jadeData, jadeConfig) {
    var jadeSource, options;
    jadeSource = fs.readFileSync(path.resolve(this.root, jadePath));
    jadeData = this.data(jadeData);
    if (jadeConfig != null) {
      jadeData.data.config = JSON.stringify(jadeConfig);
    }
    if (jadePath.match(/.jade/g)) {
      options = extend(this.config, {
        filename: jadePath
      });
      return compiler.compile(jadeSource, options)(jadeData);
    } else {
      return jadeSource;
    }
  };

  PageBuilder.prototype.data = function(a) {
    if (typeof a === 'string') {
      return JSON.parse(fs.readFileSync(path.resolve(this.root, a)));
    }
    return a;
  };

  return PageBuilder;

})(Builder);

module.exports = PageBuilder;
