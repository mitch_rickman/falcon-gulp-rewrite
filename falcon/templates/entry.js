relyjs.autoResolve([<%= supportedExtensions %>]);
<%= mappings %>
<% if(isRelease) { %>
<%= content %>
<% if(entryExtension === '.jade') { %>
require('main');
<% } else { %>
require('<%= entryName %>');
<% } %>
<% } else { %>
relyjs.load([<%= dependencies %>], function(){
  <% if(entryExtension === '.jade') { %>
  require('main');
  <% } else { %>
  require('<%= entryName %>');
  <% } %>
});
<% } %>