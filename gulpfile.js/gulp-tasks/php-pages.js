var config = require('../config');
var gulp = require('gulp');
var path = require('path');
var plugins = require('gulp-load-plugins')();

gulp.task('pages', function() {
  return gulp.src([path.join(config.srcRoot, 'pages', '**', '*.jade')])
    .pipe(plugins.data(function(file) {
      return {
        page: path.basename(file.path, '.jade'),
        isPhp: true,
        isRelease: function() {
          return config.isRelease;
        },
        "export": function() {
          return true;
        }
      };
    }))
    .pipe(plugins.jade({
      pretty: !config.isRelease,
      basedir: config.srcRoot
    }))
    .pipe(plugins.rename(function(path) {
      return path.extname = '.blade.php';
    }))
    .pipe(gulp.dest(path.join(config.php.root, config.php.template.root, config.php.template.pages)));
});
