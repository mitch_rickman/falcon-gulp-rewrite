var config = require('../config');
var gulp = require('gulp');
var path = require('path');
var plugins = require('gulp-load-plugins')();

gulp.task('icons', function(cb) {
  return gulp.src(path.join(config.srcRoot, 'icons', '*.svg'))
  .pipe(plugins.iconfont({
    fontName: 'iconfont',
    appendCodepoints: true,
    normalize: true
  })).on('codepoints', function(codepoints, options) {
    var jadeData;
    console.log(codepoints, options);
    if (!config.isRelease) {
      console.log('writing jade');
      jadeData = {
        codepoints: codepoints,
        options: options,
        isRelease: function() {
          return false;
        },
        isDebug: function() {
          return true;
        }
      };
      gulp.src(path.join(config.srcRoot, 'core', 'html', 'icons.jade'))
      .pipe(plugins.jade({
        locals: jadeData
      }))
      .pipe(gulp.dest(path.join(config.srcRoot, 'core', 'html', 'partials')));
    }
    return gulp.src(path.join(config.srcRoot, 'core', 'styles', '_icons-template.scss'))
    .pipe(plugins.consolidate('lodash', {
      glyphs: codepoints,
      fontName: 'iconfont',
      fontPath: '/assets/fonts/',
      className: 's'
    }))
    .pipe(plugins.rename('_icons.scss')).pipe(gulp.dest(path.join(config.srcRoot, 'core', 'styles')));
  })
  .pipe(gulp.dest(path.join(config.output, 'assets', 'fonts')));
});
