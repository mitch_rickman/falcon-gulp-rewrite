var gulp = require('gulp');
var lazypipe = require('lazypipe');
var runSequence = require('run-sequence');

require('./clean');

gulp.task('default', ['build']);

gulp.task('build', function() {
  return runSequence('clean', ['bundle']);
});
