var gulp = require('gulp');
var runSequence = require('run-sequence');

require('./php-clean');
require('./move-assets');
require('./php-modules');
require('./php-pages');

gulp.task('move', function() {
  return runSequence('cleanphp', ['moveassets', 'modules', 'pages']);
});
