var config = require('../config');
var gulp = require('gulp');
var path = require('path');
var del = require('del');

gulp.task('cleanphp', function(callback) {
  return del([
    path.join(config.php.assets + '**'),
    path.join(config.php.template.root, config.php.template.modules),
    path.join(config.php.template.root, config.php.template.pages)
  ], callback);
});
