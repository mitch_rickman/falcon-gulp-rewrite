var config = require('../config');
var gulp = require('gulp');
var path = require('path');
var plugins = require('gulp-load-plugins')();

gulp.task('watchphp', function() {
  return gulp.watch([
    path.join(config.srcRoot, 'modules', '**', '*.jade'),
    path.join(config.srcRoot, 'pages', '**', '*.jade')
  ],
  ['modules', 'pages']);
});
