var config = require('../config');
var gulp = require('gulp');
var path = require('path');

gulp.task('moveassets', function() {
  return gulp.src(path.join(config.output, 'assets', '**'))
    .pipe(gulp.dest(path.join(config.php.assets)));
});
