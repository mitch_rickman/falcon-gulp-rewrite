var config = require('../config');
var gulp = require('gulp');
var path = require('path');
var fs = require('fs');
var plugins = require('gulp-load-plugins')();

gulp.task('modules', function() {
  return gulp.src([path.join(config.srcRoot, 'modules', '**', '*.jade')])
    .pipe(plugins.data(function(file) {
      return {
        data: JSON.parse(fs.readFileSync(require('path').dirname(file.path) + '/data/default.json')),
        isPhp: true,
        isRelease: function() {
          return config.isRelease;
        }
      };
    }))
    .pipe(plugins.jade({
      pretty: !config.isRelease,
      basedir: config.srcRoot
    }))
    .pipe(plugins.rename(function(path) {
      return path.extname = '.blade.php';
    }))
    .pipe(gulp.dest(path.join(config.php.root, config.php.template.root, config.php.template.modules)));
});
