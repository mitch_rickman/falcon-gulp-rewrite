// Generated on 2016-07-15 using
// @14four/generator-excalibur 0.1.15
var config = require('./config');
var gulp = require('gulp');
var requireDir = require('require-dir');

// import gulp tasks
requireDir('./gulp-tasks', {
  recursive: true
});

// set up falcon
falconConfig = require('./falcon.config');
var Falcon = require('@14four/falcon');
Falcon.configure(falconConfig);

// compile with falcon
gulp.task('bundle', ['icons'], function() {
  return Falcon.start();
});
