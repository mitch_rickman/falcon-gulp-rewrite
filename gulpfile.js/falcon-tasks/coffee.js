var config = require('../config');
var lazypipe = require('lazypipe');
var plugins = require('gulp-load-plugins')();

module.exports = lazypipe()
  .pipe(plugins.coffeelint)
  .pipe(plugins.coffeelint.reporter)
  .pipe(function() {
    return plugins["if"](!config.isRelease, plugins.sourcemaps.init());
  })
  .pipe(plugins.coffee).pipe(function() {
    return plugins["if"](!config.isRelease, plugins.sourcemaps.write(config.sourcemaps));
  });
