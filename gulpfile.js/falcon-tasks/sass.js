var config = require('../config');
var lazypipe = require('lazypipe');
var plugins = require('gulp-load-plugins')();

module.exports = lazypipe()
  .pipe(function() {
    return plugins["if"](!config.isRelease, plugins.sourcemaps.init());
  })
  .pipe(plugins.sass, config.sass)
  .pipe(plugins.autoprefixer, config.autoprefixer)
  .pipe(function() {
    return plugins["if"](!config.isRelease, plugins.sourcemaps.write(config.sourcemaps));
  });
