var lazypipe = require('lazypipe');
var plugins = require('gulp-load-plugins')();

module.exports = lazypipe().pipe(plugins.uglify);
