var config = require('../config');
var lazypipe = require('lazypipe');
var plugins = require('gulp-load-plugins')();

module.exports = lazypipe()
  .pipe(plugins.eslint)
  .pipe(plugins.eslint.format).pipe(function() {
    return plugins["if"](!config.isRelease, plugins.sourcemaps.init());
  })
  .pipe(plugins.babel).pipe(function() {
    return plugins["if"](!config.isRelease, plugins.sourcemaps.write(config.sourcemaps));
  });
