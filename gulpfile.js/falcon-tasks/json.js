var lazypipe = require('lazypipe');
var plugins = require('gulp-load-plugins')();

module.exports = lazypipe()
  .pipe(plugins.jshint, {
    indent: 2
  })
  .pipe(plugins.jshint.reporter, 'jshint-stylish');
