



<!DOCTYPE html>
<!--[if lt IE 8 ]>  <html class="no-js ie lt-ie10 lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>  <html class="no-js ie lt-ie10 lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>  <html class="no-js ie lt-ie10" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
  <!--<![endif]-->
  <head>
    <title>Gulp rewrite</title>    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <script>
      /* Modernizr 2.7.1 (Custom Build) | MIT & BSD
       * Build: http://modernizr.com/download/#-csstransitions-prefixed-testprop-testallprops-domprefixes
       */
      ;window.Modernizr=function(a,b,c){function w(a){i.cssText=a}function x(a,b){return w(prefixes.join(a+";")+(b||""))}function y(a,b){return typeof a===b}function z(a,b){return!!~(""+a).indexOf(b)}function A(a,b){for(var d in a){var e=a[d];if(!z(e,"-")&&i[e]!==c)return b=="pfx"?e:!0}return!1}function B(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:y(f,"function")?f.bind(d||b):f}return!1}function C(a,b,c){var d=a.charAt(0).toUpperCase()+a.slice(1),e=(a+" "+m.join(d+" ")+d).split(" ");return y(b,"string")||y(b,"undefined")?A(e,b):(e=(a+" "+n.join(d+" ")+d).split(" "),B(e,b,c))}var d="2.7.1",e={},f=b.documentElement,g="modernizr",h=b.createElement(g),i=h.style,j,k={}.toString,l="Webkit Moz O ms",m=l.split(" "),n=l.toLowerCase().split(" "),o={},p={},q={},r=[],s=r.slice,t,u={}.hasOwnProperty,v;!y(u,"undefined")&&!y(u.call,"undefined")?v=function(a,b){return u.call(a,b)}:v=function(a,b){return b in a&&y(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=s.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(s.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(s.call(arguments)))};return e}),o.csstransitions=function(){return C("transition")};for(var D in o)v(o,D)&&(t=D.toLowerCase(),e[t]=o[D](),r.push((e[t]?"":"no-")+t));return e.addTest=function(a,b){if(typeof a=="object")for(var d in a)v(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof enableClasses!="undefined"&&enableClasses&&(f.className+=" "+(b?"":"no-")+a),e[a]=b}return e},w(""),h=j=null,e._version=d,e._domPrefixes=n,e._cssomPrefixes=m,e.testProp=function(a){return A([a])},e.testAllProps=C,e.prefixed=function(a,b,c){return b?C(a,b,c):C(a,"pfx")},e}(this,this.document);
    </script>
    <script>
      var relyjs,define,require;
      (function(e){var q=function(){};q.prototype=function(h){var n={},r={},k={},l=[],p=[],t=function(a){return"[object Function]"===Object.prototype.toString.call(a)},v=function(a,c,b){void 0===b&&(b=0);if("indexOf"in Array.prototype)return a.indexOf(c,b);0>b&&(b+=a.length);0>b&&(b=0);for(var d=a.length;b<d;b++)if(b in a&&a[b]===c)return b;return-1},w=function(a,c){if(a&&0<a.length&&"."===a[0]){var b=a.split("/"),d=c.split("/"),f=c.lastIndexOf("/")===c.length-1,f=d.length-(f?1:2),m=[],e="",g;for(g=0;g<
      b.length;g++)".."===b[g]?f--:"."!==b[g]&&m.push(b[g]);if(0>f)throw Error("Invalid Relative Path: "+a+" from "+c);for(g=0;g<=f;g++)e+=d[g]+"/";return e+=m.join("/")}return a},u=function(a,c){var b=!1,d=document.createElement("script");d.src=a;d.onload=d.onreadystatechange=function(){if(d.readyState&&"complete"!==d.readyState&&"loaded"!==d.readyState||b)return!1;d.onload=d.onreadystatechange=null;b=!0;c&&c()};void 0!==document.body&&null!==document.body?document.body.appendChild(d):document.head.appendChild(d)};
      return{constructor:q,configure:function(a){for(var c in a.externals)k[c]=a.externals[c]},autoResolve:function(a){p=a},map:function(a,c){n[a]=c},require:function(a){"undefined"!==typeof n[a]&&(a=n[a]);a=w(a,l[l.length-1]);if(0<=v(l,a))throw Error("Circular Dependency: "+l.join(" -> ")+" -> "+a);if("undefined"===typeof k[a]){var c=r[a],b={},d={exports:b};if("undefined"===typeof c||!t(c)){var f=!0;if(0<l.length)for(var m=0;m<p.length;m++)if(c=r[a+"."+p[m]],"undefined"!==typeof c&&t(c)){a+="."+p[m];f=
      !1;break}if(f)throw Error("Undefined Dependency: "+a);}l.push(a);var h=!1,g;(function(){var f=define;define=function(a,b){g=b;h=!0};k[a]=c(e.require,b,d);define=f})();h?(f=g(e.require,b,d),k[a]=f):void 0===k[a]&&(k[a]=d.exports?d.exports:b);l.pop()}return k[a]},define:function(a,c){r[a]=c},load:function(a,c){if("[object Array]"===Object.prototype.toString.call(a))for(var b=a.length,d=setTimeout(function(){if(0<b)throw Error("RelyJS Timeout: "+a);},15E3),f=function(){b--;1>b&&(clearTimeout(d),c&&c())},
      e=0;e<a.length;e++)u(a[e],f);else u(a,c)}}}();e.relyjs||(e.relyjs=new q,e.require=function(h){return e.relyjs.require(h)},e.define=function(h,n){e.relyjs.define(h,n)})})(this);
    </script>
    <!--[if lt IE 9 ]> <script src="assets/bundle/entry/polyfills.js"></script> <![endif]-->
    <link rel="stylesheet" href="assets/bundle/entry/index.css">
  </head>
  <body class="page-index">
    <h1>Gulp </h1>@include('frontend.modules.test.test', [])
    <script src="assets/bundle/entry/index.js"></script>
  </body>
</html>