




<div data-module="style-guide" class="style-guide">
  <div class="container">
    <div class="grid">
      <div class="span12"><br/>
        <h1>Style Guide</h1>
        <hr/><br/>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="grid">
      <div class="row mobile-half">
        <div class="span6">
          <h4><a href="#headlines">Headlines</a></h4>
        </div>
        <div class="span6">
          <h4><a href="#copy">Body Copy</a></h4>
        </div>
      </div>
      <div class="row mobile-half">
        <div class="span6">
          <h4><a href="#buttons">Buttons</a></h4>
        </div>
        <div class="span6">
          <h4><a href="#icons">Icons</a></h4>
        </div>
      </div>
      <div class="row mobile-half">
        <div class="span6">
          <h4><a href="#grid">Grid</a></h4>
        </div>
      </div>
      <hr/>
    </div>
  </div>
  <div id="headlines" class="container space">
    <div class="grid">
      <div class="row">
        <div class="span12 demo">
          <h2>Headlines</h2>
          <hr/>
        </div>
      </div>
    </div>
    <div>Edit these to fit the sizes for your project</div>
    <div class="grid">
      <div class="row">
        <div class="span12 demo">
          <h4 class="uncap cls">.t1</h4>
          <p class="t1">Donec rhoncus turpis mi, id elementum.</p>
          <p class="info visible-desktop">48px Regular / 48px leading</p>
          <p class="info visible-phone">32px Regular / 32px leading</p>
        </div>
      </div>
    </div>
    <div class="grid">
      <div class="row">
        <div class="span12 demo">
          <h4 class="uncap cls">.t2</h4>
          <p class="t2">Donec rhoncus turpis mi, id elementum.</p>
          <p class="info visible-desktop">36px Regular / 36px leading</p>
          <p class="info visible-phone">24px Regular / 24px leading</p>
        </div>
      </div>
    </div>
    <div class="grid">
      <div class="row">
        <div class="span12 demo">
          <h4 class="uncap cls">.t3</h4>
          <p class="t3">Donec rhoncus turpis mi, id elementum.</p>
          <p class="info visible-desktop">32px Regular / 32px leading</p>
          <p class="info visible-phone">20px Regular / 20px leading</p>
        </div>
      </div>
    </div>
    <div class="grid">
      <div class="row">
        <div class="span12 demo">
          <h4 class="uncap cls">.t4</h4>
          <p class="t4">Donec rhoncus turpis mi, id elementum.</p>
          <p class="info visible-desktop">24px Regular / 24px leading</p>
          <p class="info visible-phone">18px Regular / 18px leading</p>
        </div>
      </div>
    </div>
    <div class="grid">
      <div class="row">
        <div class="span12 demo">
          <h4 class="uncap cls">.t5</h4>
          <p class="t5">Donec rhoncus turpis mi, id elementum.</p>
          <p class="info visible-desktop">18px Regular / 18px leading</p>
          <p class="info visible-phone">16px Regular / 16px leading</p>
        </div>
      </div>
    </div>
    <div class="grid">
      <div class="row">
        <div class="span12 demo">
          <h4 class="uncap cls">.t6</h4>
          <p class="t6">Donec rhoncus turpis mi, id elementum.</p>
          <p class="info visible-desktop">16px Regular / 16px leading</p>
          <p class="info visible-phone">16px Regular / 16px leading</p>
        </div>
      </div>
    </div>
  </div>
  <div id="copy" class="container space">
    <div class="grid">
      <div class="row">
        <div class="span12 demo">
          <h2>Body Copy</h2>
          <hr/>
        </div>
      </div>
    </div>
    <div class="grid">
      <div class="row">
        <div class="span12 demo">
          <h4 class="uncap cls">.p1</h4>
          <p class="p1">Donec rhoncus turpis mi, id elementum nunc dapibus eu. Suspendisse varius tempor elit vitae vehicula. Praesent volutpat ultrices quam. Vivamus cursus enim dolor, non venenatis ligula sollicitudin in. Donec rhoncus turpis mi, id elementum nunc dapibus eu. Suspendisse varius tempor elit vitae vehicula. Praesent volutpat ultrices quam. Vivamus cursus enim dolor, non venenatis ligula sollicitudin in.</p>
          <p class="info visible-desktop">16px Regular / 20px leading</p>
          <p class="info visible-phone">14px Regular / 17px leading</p>
        </div>
      </div>
    </div>
    <div class="grid">
      <div class="row">
        <div class="span12 demo">
          <h4 class="uncap cls">.p2</h4>
          <p class="p2">Donec rhoncus turpis mi, id elementum nunc dapibus eu. Suspendisse varius tempor elit vitae vehicula. Praesent volutpat ultrices quam. Vivamus cursus enim dolor, non venenatis ligula sollicitudin in. Donec rhoncus turpis mi, id elementum nunc dapibus eu. Suspendisse varius tempor elit vitae vehicula. Praesent volutpat ultrices quam. Vivamus cursus enim dolor, non venenatis ligula sollicitudin in.</p>
          <p class="info visible-desktop">15px Regular / 18px leading</p>
          <p class="info visible-phone">13px Regular / 16px leading</p>
        </div>
      </div>
    </div>
    <div class="grid">
      <div class="row">
        <div class="span12 demo">
          <h4 class="uncap cls">.p3</h4>
          <p class="p3">Donec rhoncus turpis mi, id elementum nunc dapibus eu. Suspendisse varius tempor elit vitae vehicula. Praesent volutpat ultrices quam. Vivamus cursus enim dolor, non venenatis ligula sollicitudin in. Donec rhoncus turpis mi, id elementum nunc dapibus eu. Suspendisse varius tempor elit vitae vehicula. Praesent volutpat ultrices quam. Vivamus cursus enim dolor, non venenatis ligula sollicitudin in.</p>
          <p class="info visible-desktop">14px Regular / 17px leading</p>
          <p class="info visible-phone">12px Regular / 15px leading</p>
        </div>
      </div>
    </div>
  </div>
  <div id="buttons" class="container space">
    <div class="grid">
      <div class="row">
        <div class="span12 demo">
          <h2>Buttons</h2>
          <hr/>
        </div>
      </div>
    </div>
    <div class="grid">
      <div class="row">
        <div class="span3 demo">
          <h4 class="uncap cls">.btn</h4>
          <div><a class="btn">Learn More</a></div>
        </div>
        <div class="span3 demo">
          <h4 class="uncap cls">.btn-square</h4>
          <div><a class="btn-square">1</a></div>
        </div>
        <div class="span3 demo">
          <h4 class="uncap cls">.btn.active</h4>
          <div><a class="btn-square active">2</a></div>
        </div>
      </div>
    </div>
  </div>
  <div id="grid" class="container space">
    <div class="grid">
      <div class="row">
        <div class="span12 demo">
          <h2>Grid</h2>
          <hr/>
        </div>
      </div>
    </div>
  </div>
  <div class="container container-demo">
    <div class="grid grid-demo">
      <div class="row">
        <div class="span12 alt">
          <h4>12 Column Grid</h4>
        </div>
      </div>
      <div class="row mobile-third">
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
      </div>
      <div class="row mobile-half">
        <div class="span2">2</div>
        <div class="span2">2</div>
        <div class="span2">2</div>
        <div class="span2">2</div>
        <div class="span2">2</div>
        <div class="span2">2</div>
      </div>
      <div class="row">
        <div class="span3">3</div>
        <div class="span3">3</div>
        <div class="span3">3</div>
        <div class="span3">3</div>
      </div>
      <div class="row">
        <div class="span4">4</div>
        <div class="span4">4</div>
        <div class="span4">4</div>
      </div>
      <div class="row">
        <div class="span5">5</div>
        <div class="span7">7</div>
      </div>
      <div class="row">
        <div class="span6">6</div>
        <div class="span6">6</div>
      </div>
      <div class="row">
        <div class="span4">4</div>
        <div class="span8">8</div>
      </div>
      <div class="row">
        <div class="span3">3</div>
        <div class="span9">9</div>
      </div>
      <div class="row">
        <div class="span2">2</div>
        <div class="span10">10</div>
      </div>
      <div class="row">
        <div class="span1">1</div>
        <div class="span11">11</div>
      </div>
      <div class="row">
        <div class="span12">12</div>
      </div>
      <div class="row">
        <div class="span12 alt">
          <h4>Push</h4>
        </div>
      </div>
      <div class="row">
        <div class="span6 push3">6</div>
      </div>
      <div class="row">
        <div class="span8 push2">8</div>
      </div>
      <div class="row">
        <div class="span10 push1">10</div>
      </div>
      <div class="row">
        <div class="span4 push4">4</div>
        <div class="span3 push1">3</div>
      </div>
      <div class="row">
        <div class="span12 alt">
          <h4>Nesting</h4>
        </div>
      </div>
      <div class="row">
        <div class="span4">
          <div class="span2">2</div>
          <div class="span2">2</div>
        </div>
        <div class="span8">
          <div class="span4">4</div>
          <div class="span4">4</div>
        </div>
      </div>
      <div class="row">
        <div class="span6">
          <div class="span2">2</div>
          <div class="span4">4</div>
        </div>
        <div class="span3">
          <div class="span1">1</div>
          <div class="span1">1</div>
          <div class="span1">1</div>
        </div>
        <div class="span3">
          <div class="span2">2</div>
          <div class="span1">1</div>
        </div>
      </div>
      <div class="row">
        <div class="span12 alt">
          <h4>Squeeze</h4>
        </div>
      </div>
      <div class="row squeeze">
        <div class="span4">4</div>
        <div class="span4">4</div>
        <div class="span4">4</div>
      </div>
    </div>
  </div>
  <div class="container full-bleed container-demo">
    <div class="grid grid-demo">
      <div class="row">
        <div class="span12 alt">
          <h4>Full Bleed Container</h4>
        </div>
      </div>
      <div class="row">
        <div class="span4">4</div>
        <div class="span4">4</div>
        <div class="span4">4</div>
      </div>
    </div>
    <div class="grid grid-demo padded">
      <div class="row">
        <div class="span12 alt">
          <h4>Padded</h4>
        </div>
      </div>
      <div class="row">
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
        <div class="span1">1</div>
      </div>
    </div>
  </div>
  <div class="container full-bleed no-max container-demo">
    <div class="grid grid-demo">
      <div class="row">
        <div class="span12 alt">
          <h4>Full Bleed Container / No Max Width</h4>
        </div>
      </div>
      <div class="row">
        <div class="span4">4</div>
        <div class="span4">4</div>
        <div class="span4">4</div>
      </div>
    </div>
  </div>
</div>