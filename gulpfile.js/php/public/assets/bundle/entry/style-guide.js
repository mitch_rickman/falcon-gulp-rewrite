relyjs.autoResolve(['js','coffee']);
relyjs.map('polyfills', 'src/core/scripts/polyfills.js');
relyjs.map('main', 'src/core/scripts/main.js');
relyjs.map('jquery', 'node_modules/jquery/dist/jquery.js');
relyjs.map('test', 'src/modules/test/test.js');
relyjs.map('style-guide', 'src/modules/style-guide/style-guide.js');

define("src/modules/style-guide/style-guide.js",function(e,n,t){return function(){"use strict";var n=function(e,n){if(!(e instanceof n))throw new TypeError("Cannot call a class as a function")},i=e("jquery"),s=e("browser/log")("module"),o=function r(e){n(this,r),this.$el=i(e),s("StyleGuide : Initialized")};t.exports=o}()});

require('main');

