relyjs.autoResolve(['js','coffee']);
relyjs.map('polyfills', 'src/core/scripts/polyfills.js');
relyjs.map('main', 'src/core/scripts/main.js');
relyjs.map('jquery', 'node_modules/jquery/dist/jquery.js');
relyjs.map('test', 'src/modules/test/test.js');
relyjs.map('style-guide', 'src/modules/style-guide/style-guide.js');

define("src/modules/test/test.js",function(t,n,e){return function(){"use strict";var n=function(){function t(t,n){for(var e in n){var i=n[e];i.configurable=!0,i.value&&(i.writable=!0)}Object.defineProperties(t,n)}return function(n,e,i){return e&&t(n.prototype,e),i&&t(n,i),n}}(),i=function(t,n){if(!(t instanceof n))throw new TypeError("Cannot call a class as a function")},o=t("jquery"),r=t("browser/log")("module:test"),s=function(){function t(n,e){i(this,t),this.$el=o(n),r("Test : Initialized"),this.setBinds()}return n(t,{setBinds:{value:function(){console.log("setting binds")}}}),t}();e.exports=s}()});

require('main');

