define('src/core/scripts/polyfills.js', function(require, exports, module) { return (function(){"use strict";

define("polyfills", function (require) {
  require("bower_components/nwmatcher/src/nwmatcher.js");
  require("bower_components/selectivizr/selectivizr.js");
}); 
})()});