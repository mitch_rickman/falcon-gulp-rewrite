define('src/modules/test/test.js', function(require, exports, module) { return (function(){
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var key in props) { var prop = props[key]; prop.configurable = true; if (prop.value) prop.writable = true; } Object.defineProperties(target, props); } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var $ = require("jquery");
var log = require("browser/log")("module:test");

/**
 * Test
 *
 * @module test
 * @version 0.1.0
 * @author mitch_rickman
 *
 * Generated on 2016-07-15 using @14four/generator-excalibur 0.1.15
 */

var Test = (function () {
  function Test(element, config) {
    _classCallCheck(this, Test);

    this.$el = $(element);
    log("Test : Initialized");

    this.setBinds();
  }

  _createClass(Test, {
    setBinds: {
      value: function setBinds() {
        console.log("setting binds");
      }
    }
  });

  return Test;
})();

module.exports = Test; 
})()});