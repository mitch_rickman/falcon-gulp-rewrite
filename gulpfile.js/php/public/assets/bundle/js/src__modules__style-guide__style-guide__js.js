define('src/modules/style-guide/style-guide.js', function(require, exports, module) { return (function(){"use strict";

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var $ = require("jquery");
var log = require("browser/log")("module");

/**
 * Project Style Guide
 *
 * @module style-guide
 * @version 0.1.0
 * @author 14Four
 *
 * Generated on 2016-07-15 using @14four/generator-excalibur 0.1.15
 */

var StyleGuide = function StyleGuide(element) {
  _classCallCheck(this, StyleGuide);

  this.$el = $(element);
  log("StyleGuide : Initialized");
};

module.exports = StyleGuide; 
})()});