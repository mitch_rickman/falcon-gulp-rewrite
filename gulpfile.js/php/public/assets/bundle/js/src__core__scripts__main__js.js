define('src/core/scripts/main.js', function(require, exports, module) { return (function(){"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var key in props) { var prop = props[key]; prop.configurable = true; if (prop.value) prop.writable = true; } Object.defineProperties(target, props); } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

// Uncomment if you require ES6 Polyfills
// require("node_modules/babel-polyfill/dist/polyfill.min.js");

define("main", function (require) {

  var $ = require("jquery");
  var log = require("browser/log")("main");

  var Main = (function () {
    function Main() {
      _classCallCheck(this, Main);

      // initialize all components with data-component attributes
      this.init("component");
      // initialize all modules with data-module attributes
      this.init("module");
    }

    _createClass(Main, {
      init: {
        value: function init(attrType) {
          var _this = this;

          this[attrType] = [];
          var $elements = $("[data-" + attrType + "]");
          if ($elements.length) {
            $elements.each(function (idx, elem) {
              var $el = $(elem);
              var name = $el.attr("data-" + attrType);
              var config = $el.attr("data-config");
              if (config && config.length > 0) {
                config = JSON.parse(config);
              }
              var Definition = require(name);
              log("Main : Initializing " + attrType + " : " + name);
              _this[attrType].push(new Definition(elem, config));
            });
          }
        }
      }
    });

    return Main;
  })();

  // Singleton
  return new Main();
}); 
})()});