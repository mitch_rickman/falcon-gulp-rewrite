var config = require('./config');
var requireDir = require('require-dir');
var path = require('path');
var PageBuilder = require('../falcon/page-builder');

var tasks = requireDir('./falcon-tasks', {
  recursive: true,
  camelcase: true
});

module.exports = {
  isRelease: config.isRelease,
  root: config.root,
  srcRoot: config.srcRoot,
  buildRoot: config.output,
  server: {
    port: 4002,
    liveReloadPort: 35729 + 4002,
    catchAll: 'index.html'
  },
  assembler: {
    root: config.output,
    projectRoot: config.root,
    dest: path.join(config.output, 'assets', 'bundle', 'entry'),
    jsTemplatePath: path.resolve(__dirname, '../falcon/templates/entry.js'),
    commonThreshold: 2,
    commonName: 'common',
    releaseTasks: {
      js: tasks.jsRelease,
      css: tasks.cssRelease
    }
  },
  resolver: {
    root: config.root,
    extensions: ['.js', '.coffee', '.scss', '.sass', '.jpg', '.png', '.gif'],
    paths: [path.resolve(__dirname, '../'), path.resolve(__dirname, '../src/modules'), path.resolve(__dirname, '../src/core/styles'), path.resolve(__dirname, '../node_modules/@14four/armory-scripts/lib'), path.resolve(__dirname, '../node_modules/@14four/armory-components/lib')]
  },
  notifier: {
    projectName: 'Gulp rewrite'
  },
  triggers: ['../../_*.scss', '../../src/modules/**/*.jade', '../../src/core/**/*.jade', '../../src/**/*.json'],
  processors: {
    asset: {
      test: './src/assets/**/*.*',
      src: './src/assets/**/*.*',
      base: './src/assets/',
      dest: path.join(config.output, 'assets'),
      isEntry: true
    },
    sass: {
      test: '*.scss',
      filter: ['**', '!**/_*.scss'],
      task: tasks.sass,
      dest: path.join(config.output, 'assets', 'bundle', 'css'),
      preAnalyze: true
    },
    coffee: {
      test: '*.coffee',
      task: tasks.coffee,
      dest: path.join(config.output, 'assets', 'bundle', 'js'),
      postAnalyze: true,
      amdify: true
    },
    js_armory: {
      test: './node_modules/@14four/armory-scripts/lib/**/*.js',
      task: tasks.js,
      dest: path.join(config.output, 'assets', 'bundle', 'js'),
      postAnalyze: true,
      amdify: true,
      mapRoots: ['node_modules/@14four/armory-scripts/lib/']
    },
    js_components_vendor: {
      test: './node_modules/@14four/armory-components/lib/**/+(node_modules|bower_components)/**/*.js',
      dest: path.join(config.output, 'assets', 'bundle', 'js'),
      amdify: true
    },
    js_components: {
      test: './node_modules/@14four/armory-components/lib/*/*.js',
      task: tasks.js,
      dest: path.join(config.output, 'assets', 'bundle', 'js'),
      postAnalyze: true,
      amdify: true,
      mapRoots: ['node_modules/@14four/armory-components/lib/']
    },
    js_vendor: {
      test: '+(node_modules|bower_components)/**/*.js',
      dest: path.join(config.output, 'assets', 'bundle', 'js'),
      amdify: true
    },
    polyfills: {
      test: 'src/core/scripts/polyfills.js',
      src: 'src/core/scripts/polyfills.js',
      task: tasks.js,
      dest: path.join(config.output, 'assets', 'bundle', 'js'),
      postAnalyze: true,
      amdify: true,
      isEntry: true
    },
    js: {
      test: '*.js',
      task: tasks.js,
      dest: path.join(config.output, 'assets', 'bundle', 'js'),
      postAnalyze: true,
      amdify: true
    },
    json: {
      test: '*.json',
      task: tasks.json,
      dest: false
    },
    pages: {
      test: 'src/pages/*.jade',
      src: 'src/pages/*.jade',
      dest: config.output,
      isEntry: true,
      builder: new PageBuilder(config.isRelease, config.srcRoot, ['core/scripts/main.js', 'core/styles/main.scss'])
    }
  }
};
