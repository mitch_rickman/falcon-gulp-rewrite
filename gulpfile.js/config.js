var path = require('path');
var argv = require('minimist')(process.argv.slice(2));

module.exports = {
  isRelease: argv.release === true,
  root: path.resolve(__dirname + "/../"),
  srcRoot: path.resolve(__dirname, '../src'),
  output: argv.release === true ? path.join('build', 'release') : path.join('build', 'debug'),
  php: {
    root: path.resolve(__dirname, '../', 'php'),
    assets: path.resolve(__dirname, 'php', 'public', 'assets'),
    template: {
      root: 'resources/views/frontend',
      modules: 'modules',
      pages: 'pages'
    }
  },
  sass: {
    outputStyle: argv.release === true ? 'compressed' : 'expanded',
    includePaths: [path.resolve(__dirname, '../node_modules'), path.resolve(__dirname, '../src/core/sass'), path.resolve(__dirname, '../node_modules/@14four/armory-styles')],
    autoprefixer: {
      browsers: ['last 4 versions', 'Android 4']
    },
    sourcemaps: {
      includeContent: true
    }
  }
};
