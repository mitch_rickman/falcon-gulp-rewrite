
'use strict';

let $ = require('jquery');
let log = require('browser/log')('module:test');

/**
 * Test
 *
 * @module test
 * @version 0.1.0
 * @author mitch_rickman
 *
 * Generated on 2016-07-15 using @14four/generator-excalibur 0.1.15
 */

class Test {

  constructor(element, config) {
    this.$el = $(element);
    log('Test : Initialized');

    this.setBinds();
  }

  setBinds() {
    console.log("setting binds");
  }
}

module.exports = Test;
